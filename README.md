# Quietus, The Audiobook Player with a Sinister Name

## Explanation

This audiobook player exists for one purpose: to remember where you
started your audiobook from.

Programs like VLC will remember where you "left off" your audiobook.
But the fact is you fell asleep while you were listening. You may well
have intended to.

This means where you "left off" is at least eight hours off.  Why
can't (I asked myself, over and over again, on those sleepless nights,
tossing and turning) -- why can't it remember where I STARTED?

Actually, in writing this program I belatedly realised why. How _can_
it know? At this point, this program certainly doesn't. You have to
tell it what your bookmarks are. You do that with 'B'.

'n' skips to the next bookmark, 'p' the previous. 'l' skips forward by
\-\-short-skip-ms, 'k' by --long-skip-ms. 'h' and 'k' do the same but
backwards. You can set the volume in decibles with --volume.

There'll be more, but not much more. Eventually I'll write a little
algorithm to automatically figure out out where you "started".
Probably start a timer upon every seek and if you listen for longer
than twenty minutes, set the bookmark.

There'll be other solutions to this audiobook problem. I swear I found
something once, some small project.

I'm saving the bookmarks as an 'abp' file in the file's directory.
Eventually I'll probably use .cue files alongside a bookmark for books
split into smaller files.

And I might support .m4b. So far I suppose all the formats libsndfile
does. .m4b isn't open source. There'll be a library for it somewhere.
I'm not sure if there's an open source m4b equivalent. I'll find it if
there is.

I'm talking about .m4b because it can store bookmarks within the file,
which is obviously better than writing files to your directories.
Maybe instead I could save the files to, eg, ~/.local/share/quietus.

## Limitations

### File formats

I'm using libsndfile to read the files. That's properly open source,
which means no mp3, m4b, etc. You'll most likely have to convert
audiobooks to use it, which can take ages.

If you want to use ffmpeg to convert files you'll need the non-free
bits of it. In Fedora that means you need rpmfusion. In other distros
you might get the non-free stuff automatically.

Here's a command to convert from whatever to ogg vorbis.

```
ffmpeg -i audiobook.m4b -vn audiobook.ogg
```

Here's a website that explains the basics of ffmpeg well:
https://opensource.com/article/17/6/ffmpeg-convert-media-file-formats

### Seeking

Seeking is very slow on enormous files. I wonder why? I don't know
yet.

## Install

I've written this on Linux. It might not work elsewhere without
modification.

```
git clone --recursive https://gitlab.com/objection/quietus.git
meson build
sudo ninja -Cbuild install
```
