#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>
#include <useful.h>
#include <portaudio.h>
#include <err.h>
#include <ncurses.h>
#include <argp.h>
#include <time.h>
#include <stdarg.h>
#include <math.h>
#include "cwalk.h"
#include "../lib/darr/darr.h"
#include "../lib/time_string/time_string.h"
#define FRAMES_PER_BUFFER 256
#define N_SCRATCH PATH_MAX

#define CTRL(c) ((c) & 037)
#define each(_item, _arr, _n) \
	for (typeof (_arr[0]) *_item = _arr; _item < _arr + _n; _item++)
#define beach(_item, _arr, _n) \
	for (typeof (_arr[0]) *_item = _arr + _n - 1; _item >= _arr; _item--)

char scratch[N_SCRATCH];

DECLARE_ARR (size_t, size_t);
DEFINE_ARR (size_t, size_t);

struct buffer {
	SNDFILE *sndfile;
	SF_INFO sf_info;
	int channels;
	// Where you're skipping or not.
	bool seeking;
	bool playing;
	// I need to keep track of the frame because libsndfile
	// doesn't provide an ftell function. There'll be another way.
	// I don't like doing it this. Makes me paranoid I'll forget
	// to add or subtract frames.
	int64_t frame;
	char *path;
	FILE *bookmark_fp;
	// Stored in frames
	struct size_t_arr bookmarks;
};

DECLARE_ARR (struct buffer, buffer);
DEFINE_ARR (struct buffer, buffer);

struct playlist  {
	struct buffer_arr buffers;
	int i;
};

struct tui {
	WINDOW *win;
};

struct args {
	int short_skip_sec;
	int long_skip_sec;
	struct playlist playlist;
	struct {
		struct ts_time now;
	} secret;
	float volume;
} args = {
	.short_skip_sec = 3,
	.long_skip_sec = 1 * 60 * 10,
};

void strip (char *s) {
	char *p = s;
	int len = strlen (p);
	if (len == 0) return;

	while (isspace (p[len - 1])) p[--len] = 0;
	while (*p && isspace (*p)) ++p, --len;

	memmove (s, p, len + 1);
}

int portaudio_callback (const void *_input, void *_output,
		unsigned long frame_count,
		const PaStreamCallbackTimeInfo *time_info,
		PaStreamCallbackFlags status_flags, void *user_data) {
	struct playlist *playlist = user_data;
	struct buffer *buffer = (*playlist).buffers.d + (*playlist).i;
	float *output = _output;

	// This if means we dont skip and write the next frames in
	// the same go. Why? No particular reason.
	if ((*buffer).seeking) {
		for (int i = 0; i < FRAMES_PER_BUFFER * (*buffer).sf_info.channels;
				i++) {
			output[i] = 0;
		}
		return paContinue;
	}
#if 0
		// If it returns -1 you've probably gone past the end of the
		// file, so exit.
		// Really, I'd want to put in an error message if an error's
		// happened.
		if ((*buffer).frame  > (*buffer).sf_info.frames) {
			(*playlist).i++;
			(*buffer).playing = 0;
			return paComplete;
		}
		else if ((*buffer).frame < 0) (*buffer).frame = 0;
		if (sf_seek ((*buffer).sndfile, (*buffer).frame, SEEK_SET) == -1)
			// If you were skipping forward, exit
			assert (0);
		(*buffer).seeking = 0;
#endif
#if 0
	else {
#endif
		sf_count_t count = sf_readf_float ((*buffer).sndfile, output,
				FRAMES_PER_BUFFER);
		if (count == 0) {
			(*playlist).i++;
			(*buffer).playing = 0;
			return paComplete;
		}
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < (*buffer).sf_info.channels; j++)
				output[i * (*buffer).sf_info.channels + j] *= args.volume;
		}
		(*buffer).frame += count;
#if 0
	}
#endif

	return paContinue;
}

static struct tui init_ncurses (void) {

	initscr ();
	raw ();
	noecho ();
	nonl ();
	intrflush (stdscr, FALSE);
	keypad (stdscr, TRUE);
	start_color ();
	curs_set (0);
	assume_default_colors (-1,-1);
	struct tui res = {
		.win = newwin (LINES, COLS, 0, 0),
	};
	nodelay (res.win, 1);
	notimeout (res.win, 1);
	return res;
}

static void write_bookmarks (void) {
	char bookmark_path[PATH_MAX];
	each (buffer, args.playlist.buffers.d, args.playlist.buffers.n) {
		cwk_path_change_extension ((*buffer).path, "abm",
				bookmark_path, PATH_MAX);
		FILE *fp = fopen (bookmark_path, "w");
		if (!fp) assert (0);
		each (bookmark, (*buffer).bookmarks.d, (*buffer).bookmarks.n)
		{
			time_t secs = (float) *bookmark / (float)
				(*buffer).sf_info.samplerate;
			char *str = ts_string_from_duration (scratch, N_SCRATCH,
					&(struct ts_duration) {.secs = secs},
					&args.secret.now.tm, 0);
			fprintf (fp, "%s\n", str);
		}
		fclose (fp);
	}
}

// I'm not quite sure if this function actually gets the previous
// bookmark. It really gets the "current" bookmark.
static int get_current_bookmark_index (struct size_t_arr bookmarks,
		int64_t frame) {
	int res = 0;
	beach (bookmark, bookmarks.d, bookmarks.n) {
		if (*bookmark < frame) {
			res = bookmark - bookmarks.d;
			break;
		}
	}
	return res;
}

int cmp_size_t (const void *a, const void *b) {
	return *(size_t *) a - *(size_t *) b;
}

// Stolen from
// https://gist.github.com/pebble8888/9fd58c1b47cc54005cb62a9b7b5b66bb
static float db_to_linear (float db) {
    return powf (10.f, db / 20.f);
}

// Stolen from
// https://gist.github.com/pebble8888/9fd58c1b47cc54005cb62a9b7b5b66bb
static float linear_to_db (float linear) {
    return 20.f * log10f (linear);
}

static int input (struct tui *tui, struct buffer *buffer) {
	switch (wgetch ((*tui).win)) {
		case CTRL ('c'):
			return 1;
		case 'b':
			(*buffer).seeking = 1;
			(*buffer).frame += -((*buffer).sf_info.samplerate *
				args.long_skip_sec);
			break;
		case 'B':
			size_t_arr_push (&(*buffer).bookmarks, (*buffer).frame);
			$qsort ((*buffer).bookmarks.d, (*buffer).bookmarks.n,
					cmp_size_t);
			break;
		case 'h':
			//  NOTE: these calculations could happen in  an update
			//  function. Skip could be = args.short_skip_sec.
			//  (*buffer).frame could be added to in update.
			(*buffer).seeking = 1;
			(*buffer).frame += -((*buffer).sf_info.samplerate *
				args.short_skip_sec);
			break;
		case 'Q':
			(*buffer).seeking = 1;
			(*buffer).frame = (*buffer).sf_info.frames / 10;
			break;
		case 'j': {
			float db_volume = linear_to_db (args.volume);
			db_volume -= db_to_linear (0.1);
			args.volume = db_to_linear (db_volume);
			if (args.volume < 0) args.volume = 0;
		}
			break;
		case 'k': {
			float db_volume = linear_to_db (args.volume);
			db_volume += db_to_linear (0.1);
			args.volume = db_to_linear (db_volume);
			if (args.volume > 1) args.volume = 1;
		}
			break;
		case 'l':
			(*buffer).seeking = 1;
			(*buffer).frame += (*buffer).sf_info.samplerate *
				args.short_skip_sec;
			break;
		case 'n': {
			// get "current" book mark. If it's -1, there's no bookmark before
			// where you are.
			int i = get_current_bookmark_index ((*buffer).bookmarks,
					(*buffer).frame);
			if (i == -1 || i == (*buffer).bookmarks.n - 1) break;
			i++;
			(*buffer).frame = (*buffer).bookmarks.d[i];
			(*buffer).seeking = 1;
		}
			break;
		case 'p': {
			int i = get_current_bookmark_index ((*buffer).bookmarks,
					(*buffer).frame);
			if (i == -1) break;
			if ((*buffer).frame - (*buffer).bookmarks.d[i] <
					(*buffer).sf_info.samplerate * 1) // ie 1 second.
				if (i > 0) i--;
			(*buffer).frame = (*buffer).bookmarks.d[i];
			(*buffer).seeking = 1;
		}
			break;
		case 'w':
			(*buffer).seeking = 1;
			(*buffer).frame += (*buffer).sf_info.samplerate *
				args.long_skip_sec;
			break;

	}
	return 0;
}

static void draw (struct tui *tui, struct buffer *buffer) {
	werase ((*tui).win);
	int line = LINES / 2;
	float fraction = (float) (*buffer).frame /
		(float) (*buffer).sf_info.frames;
	float cols_fraction;
	mvwprintw ((*tui).win, line - 2, 0, "%s", (*buffer).path);
	for (int i = 0; i < COLS; i++) {
		cols_fraction = (float) i / (float) COLS;
		if (cols_fraction < fraction)
			mvwprintw ((*tui).win, line, i, "#");
		else
			mvwprintw ((*tui).win, line, i, "-");
	}

	// NOTE: this was really fucking hard for me, so excuse me as
	// I comment it carefully.
	//
	// I know it's not done in an efficient way -- I know if I knew
	// basic algebra wouldn't need the for loop.

	// The number of frames each column is worth.
	float col_frames = (float) (*buffer).sf_info.frames / (float) COLS;
	each (bookmark, (*buffer).bookmarks.d, (*buffer).bookmarks.n) {
		float bookmark_frames =
				*bookmark;
		for (int i = 0; i < COLS; i++) {
			cols_fraction = (float) i / (float) COLS;
			if (bookmark_frames > (col_frames * i)
					&& bookmark_frames < (col_frames * (i + 1))) {
				mvwprintw ((*tui).win, line - 1, i, "b");
				break;
			}
		}
	}

	mvwprintw ((*tui).win, line + 1, 0,
			"%ld", (long int) ((float) (*buffer).frame / (float)
			(*buffer).sf_info.samplerate));

	wnoutrefresh ((*tui).win);
	doupdate ();
}

static void push_bookmarks_if_there (struct buffer *buffer) {
	char bookmark_path[PATH_MAX];
	cwk_path_change_extension ((*buffer).path, "abm",
			bookmark_path, PATH_MAX);
	char *line = scratch;
	size_t n_line = N_SCRATCH;
	if (((*buffer).bookmark_fp = fopen (bookmark_path, "r"))) {
		(*buffer).bookmarks = size_t_arr_create (8);
		while (getline (&line, &n_line, (*buffer).bookmark_fp)
				!= -1) {
			strip (line);
			time_t bookmark = ts_secs_from_duration
				(ts_duration_from_duration_string
				(line, strchr (line, '\0')), &args.secret.now.tm);
			// Ignore errors
			if (!ts_errno) {
				bookmark *= (*buffer).sf_info.samplerate;
				size_t_arr_push (&(*buffer).bookmarks, bookmark);
			}
		}
	}
	$qsort ((*buffer).bookmarks.d, (*buffer).bookmarks.n, cmp_size_t);
}


static error_t parse_opt (int key, char *arg,
		struct argp_state *state) {
	switch (key) {
		case 'v': {
			char *endp;
			float db = strtof (arg,  &endp);
			if (db == 0 && endp == arg) err (1, "Bad int");
			args.volume = db_to_linear (db);
		}
			break;
		case 's':
			args.short_skip_sec = ts_secs_from_duration
				(ts_duration_from_duration_string (arg, NULL),
				 &args.secret.now.tm);
			break;
		case 'l':
			args.long_skip_sec = ts_secs_from_duration
				(ts_duration_from_duration_string (arg, NULL),
				 &args.secret.now.tm);
			break;

		case ARGP_KEY_INIT:
			args.playlist.buffers = buffer_arr_create (8);
			args.volume = 1;
			break;
		case ARGP_KEY_ARG: {
			// NOTE: it looks like I've decided to load all of the
			// buffers here at the start. Rather than just having
			// one struct buffer loading it for every track. It's
			// fine.
			struct buffer buffer = {
				.sndfile = sf_open (arg,
						SFM_READ,
						&buffer.sf_info),
			};
			if (!buffer.sndfile)
				errx (1, "Couldn't open %s: %s",
						arg, sf_strerror (buffer.sndfile));
			buffer.path = realpath (arg, NULL);
			buffer_arr_push (&args.playlist.buffers, buffer);
		}
			break;
		case ARGP_KEY_END: {
			if (!args.playlist.buffers.n) argp_usage (state);
			each (buffer, args.playlist.buffers.d, args.playlist.buffers.n)
				push_bookmarks_if_there (buffer);
		}
			break;
		default:
			break;
	}
	return 0;
}

static void q_err (char *fmt, ...) {
	va_list ap;

	va_start (ap, fmt);
	stderr = fdopen (1, "w");
	FILE *fp = fopen ("/tmp/hi", "r");
	char *buf = scratch;
	size_t n_buf = N_SCRATCH;
	getdelim (&buf, &n_buf, '\0', fp);
	endwin ();
	fprintf (stderr, "%s", buf);
	vfprintf (stderr, fmt, ap);
	va_end (ap);
	exit (0);
}

static void update (struct buffer *buffer) {
	if ((*buffer).seeking) {
		if (sf_seek ((*buffer).sndfile, (*buffer).frame, SEEK_SET) == -1)
			// If you were skipping forward, exit
			assert (0);
		(*buffer).seeking = 0;
	}
}



int main (int argc, char **argv) {
	// NOTE: this gets portaudio output off my screen. It'd be good to
	// tail the file and print the last message. But then I'd want you
	// to be able to page through it and display timestamps. I'm saying
	// I may do that at some point.
	args.secret.now = ts_time_from_timestamp (time (NULL));
	char *argp_doc = "FILES";
	char *argp_args_doc =  "Fall asleep to an audiobook";
	struct argp_option options[] = {
		{"short-skip-ms", 's',
			"TIME STRING", 0,
			"Set length of short skip"},
		{"long-skip-ms", 'l',
			"TIME STRING", 0,
			"Set length of long skip"},
		{"volume", 'v',
			"FLOAT DECIBLES", 0,
			"The volume in db"},
		{0},
	};
	struct argp argp = {
		options, parse_opt, argp_doc, NULL,
	};
	argp_parse (&argp, argc, argv, 0, 0, &args);

	// Redirect stderr to file to silence Portaudio. Portaudio spits
	// out lots of info. In future, maybe show these in a window.
	freopen ("/tmp/hi", "w", stderr);

	struct tui tui = init_ncurses ();

	PaError pa_error = Pa_Initialize ();
	if (pa_error != paNoError)
		q_err ("Couldn't initialise Portaudio: %s",
				Pa_GetErrorText (pa_error));

	while (1) {
		PaStream *stream;

		if (args.playlist.i > args.playlist.buffers.n - 1)
			args.playlist.i = args.playlist.buffers.n;
		struct buffer *buffer = args.playlist.buffers.d + args.playlist.i;
		(*buffer).playing  = 1;
		pa_error = Pa_OpenDefaultStream (&stream, 0,
				(*buffer).sf_info.channels, paFloat32,
				(*buffer).sf_info.samplerate, FRAMES_PER_BUFFER,
				portaudio_callback, &args.playlist);

		pa_error = Pa_StartStream (stream);
		if (pa_error != paNoError)
			q_err ("Couldn't open stream: %s",
					Pa_GetErrorText (pa_error));
		draw (&tui, buffer);
		while (1) {
			if (input (&tui, buffer)) goto end;
			update (buffer);
			draw (&tui, buffer);
			if (!(*buffer).playing) break;
		}

		pa_error = Pa_StopStream (stream);
		if (pa_error != paNoError)
			q_err ("Couldn't close stream: %s",
					Pa_GetErrorText (pa_error));
	}
	if (pa_error != paNoError)
		q_err ("Couldn't terminate Portaudio: %s",
				Pa_GetErrorText (pa_error));

end:;
	write_bookmarks ();
	endwin ();
	// NOTE: for now portaudio messages are ignored unless a fatal (to
	// this program) happens.
	pa_error = Pa_Terminate ();
}

// Play
// You need a slider
// So you need ncurses
// You need it to know if there's a bunch of files
// You could load a .cue file
